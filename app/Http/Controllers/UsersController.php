<?php

namespace App\Http\Controllers;

//use Request;
use \App\Models\Users;
use Illuminate\Http\Request;

class UsersController extends Controller
{
	
	public function index(){	
		
		$this_array = array(14,23,264,654,4,34,34);
		//prepare return type
		$sum = 0;
		//loop and accumulate
		foreach($this_array as $key){
			$sum = $sum + $key;			
		}
		
		$person_array = array('Leanna', 'derek', 'Lisa', 'John', 'lancelot', 'Michael', 'norman', 'Lawrence of Arabia');
		//search key L or l
		$searchkey = '';
		//found array list 
		$new_array = [];
		
		//iterate person array , substring fron index 0 to 1, 
		//search and extract for a matching character starting with L or l 
		//push found key to a new array and 
		//return json array to consumer end point ...
		foreach($person_array as $key){
			$searchkey = substr($key,0,1);				
			if($searchkey === 'L' || $searchkey === 'l' )
			{
				array_push($new_array,$key);
			}		
		}		
		$idnum=[];
		$id = "8,9,1,0,2,9,5,2,0,9,8,7,1";
		$arr = explode(',',$id);
		$idnum = array_unique($arr);
		
		$sorted = rsort($arr, SORT_NUMERIC);
		
		//$res = array_unique($arr);
		//$str= '';
		$dups  = [];
		$idnum = array_unique($arr);
		$i=0;
		$zeros=0;
		foreach($idnum as $id)
		{
			$i++;
			$dup = "dup". $i;
			 
			 array_push($dups,$id);
			 $zeros = 13 - $i;
		}
		$z=0;
		$z = $zeros;
		
		
		$zero = '0';
		$dup = " dup ";
		$k=1;
		for($j = 0 ; $j < $zeros ; $j++)
		{		
			$k++;
			$dup = "dup".($z + $k);
			 
			 array_push($dups,$zero);
		}
		
		$str = '';
		foreach($dups as $id)
		{
			 $str = $str.$id;
		}
		
				
		$iddup = implode(',',$dups);		
		$users = \App\Models\Users::paginate(2);
		
		return response()->json([
		"msg"=>"Successfully retrieved users!",
		"newarray"=>$new_array,
		"strid"=>$str,
		"dups"=>$dups,
		"sum"=>$sum,
		"users" =>$users->toArray()		
		],200
		);
	}
	/*
	public function show(Request $request, $id){		
		
		$users = \App\Models\Users::find($id);
		
		return response()->json([
		"msg"=>"Successfully retrieved users info!",
		"single users" =>$users		
		],200
		);	
	}
	
	
	public function update(Request $request, $id){
		
		$users =  \App\Models\Users::find($id);	
		
		$users->firstname  = $request->firstname; 
		$users->surname =  $request->surname;
		$users->idnumber =  $request->idnumber;
		
		$users->save();
		
		return response()->json([
		"msg"=>"Successfully edited/saved person!"
		],200
		);
	}
	*/
	public function store(Request $request){
		
		$users =  new \App\Models\Users();		
		
		$users->firstname =  $request->firstname;
		$users->idnumber =  $request->idnumber;
		$users->surname = $request->surname;
		
		$users->save();
		
		return response()->json([
		"msg"=>"Successfully saved person!"
		],200
		);
	}
	/*
	public function destroy(Request $request, $id){
		
		$user = \App\Models\Users::find($id);
		$user->delete();
		return response()->json([
		"msg"=>"Successfully user person!"
		],200
		);
	}*/
	}
	?>
	
