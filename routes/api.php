<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('RestaurantAdministrators', 'RestaurantAdminController@index');
Route::post('/RestaurantAdministrators', 'RestaurantAdminController@store');
Route::put('RestaurantAdministrators', 'RestaurantAdminController@auth_admin');
/*
Route::get('RestaurantAdministrator/{id}', 'RestaurantAdminController@show');
Route::put('RestaurantAdministrator/{id}', 'RestaurantAdminController@update');
Route::delete('RestaurantAdministrator', 'RestaurantAdminController@destroy');*/